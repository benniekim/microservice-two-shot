# Wardrobify

Team:

* Lupe Mendez - Shoes microservice
* Bennie Kim - Hats microservice

## Shoes microservice

We created a model to store the features of the each shoe instance.
In order to categorize the shoes, we created another model that stores the bin value
object from the wardrobe microservice. This allows us to be able to use bins within
our own microservice (as a foreign key for one bin and many shoes). We also had to
use a poller to pull the bins data from the wardrobe microservice. Once bins were created, we could use that data to categorize the newly created shoes.

## Hats microservice

We created a model that stores the necessary information needed for each hat instance.
Because we also needed to access location within the microservice for each instance,
another model was created so that the hats' locations could be accessed as value objects
within the hats microservice itself.

We then set up a poller that would allow the hats microservice to pull location data from
the wardrobe microservice.
As locations were created, new hats could then be created and attributed to the locations.
