from django.urls import path

from .views import api_shoe_list, api_show_shoe

urlpatterns = [
    path("bins/<int:bin_vo_id>/shoes/", api_shoe_list, name="api_shoe_list"),
    path("shoes/", api_shoe_list, name="api_create_shoe"),
    path("shoes/<int:id>/", api_show_shoe, name="api_show_shoe"),
]
