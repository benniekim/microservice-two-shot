import React from "react";
import { NavLink } from 'react-router-dom';

function ShoeList({shoes, getShoes}) {
  const deleteShoe = async (id) => {
    fetch (`http://localhost:8080/api/shoes/${id}/`, {
      method: "delete"
    })
    // .then (() => {
    //   return getShoes()
    // }).catch(console.log)
  }
  if(shoes === undefined) {
    return null;
  }

  return (
    <>
    <div className="nav-item">
      <NavLink className="nav-link" to="new">
        <button type="button" className="btn btn-primary">Create New Shoes</button>
      </NavLink>
    </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model Name</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoes => {
          return (
            <tr key={shoes.id}>
              <td>{shoes.manufacturer}</td>
              <td>{shoes.model_name}</td>
              <td>{shoes.color}</td>
              <td className="align-middle">
                <img src={shoes.url} height="100" width="100"></img></td>
              <td>
                <button type="button" className="btn btn-danger" onClick={ () => deleteShoe(shoes.id)}>Delete</button>
              </td>
            </tr>

          );
        })}

      </tbody>
      </table>
      </>
  );
}


export default ShoeList;
