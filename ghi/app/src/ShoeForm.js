import React, {useEffect, useState} from 'react';

function ShoeForm () {
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState("");
    const [modelName, setModelName] = useState("");
    const [color, setColor] = useState("");
    const [url, setUrl] = useState("");
    const [bin, setBin] = useState("");


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setBins(data.bins)
        }
      }

      useEffect(() => {
        fetchData();
      }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.manufacturer = manufacturer;
        data.model_name  = modelName;
        data.color = color;
        data.url = url;
        data.bin = bin

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        };
        const shoeResponse = await fetch(shoeUrl, fetchOptions);
        if (shoeResponse.ok) {

            setManufacturer('');
            setModelName('');
            setColor('');
            setUrl('');
            setBin('');
        }
    }


    const handleManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
        }

    const handleModelName = (event) => {
        const value = event.target.value;
        setModelName(value);
        }

    const handleColor = (event) => {
        const value = event.target.value;
        setColor(value);
        }

    const handleUrl = (event) => {
        const value = event.target.value;
        setUrl(value);
        }

    const handleBin = (event) => {
        const value = event.target.value;
        setBin(value);
        }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleManufacturer} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleModelName} value={modelName} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleUrl} value={url} placeholder="Url" required type="url" name="url" id="url" className="form-control"/>
                <label htmlFor="url">Url</label>
              </div>
              <div className="mb-3">
                <select onChange={handleBin} value={bin} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a Bin</option>
                  {bins.map(bin => {
                        return (
                        <option key={bin.href} value={bin.href}>
                            {bin.closet_name}
                        </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

export default ShoeForm;
