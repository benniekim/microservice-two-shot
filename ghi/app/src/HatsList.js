import React from "react";


function HatsList(props) {

    async function deleteHat(href) {
        const deleteUrl = `http://localhost:8090${href}`
        const deleteResponse = await fetch(deleteUrl, {method: 'delete'})
        if (deleteResponse.ok) {
            console.log('Deleted')
        }
    }




return(
    <div>
        <table className="table">
            <thead>
                <tr>
                    <th scope="col">Picture</th>
                    <th scope="col">Style name</th>
                    <th scope="col">Fabric</th>
                    <th scope="col">Color</th>
                    <th scope="col">Closet</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map((hat) => {
                    return (
                        <tr key={hat.href}>
                            <td><img src={hat.picture_url} height="200" /></td>
                            <td>{hat.style_name}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.color}</td>
                            <td>{hat.location}</td>
                            <td><button onClick={() => deleteHat(hat.href)} type="button" className="btn btn-outline-secondary btn-sm">Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    </div>
)
}

export default HatsList;
