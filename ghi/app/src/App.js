import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm';
import HatsList from './HatsList';
import ShoeForm from "./ShoeForm";
import ShoeList from "./ShoeList";
import {useState, useEffect} from 'react';


function App(props) {
  const[shoes, setShoes] = useState([])
  const getShoes = async () => {
    const shoeUrl= 'http://localhost:8080/api/shoes/';
    const shoeResponse = await fetch(shoeUrl);
    if(shoeResponse.ok){
      const data = await shoeResponse.json();
      const shoes = data.shoes
      setShoes(shoes)
      // console.log(shoes);
    }
  }

  useEffect(() => {getShoes();}, [])
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" element={<HatsList hats={props.hats}/>}/>
          <Route path="hats">
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path='/shoes/'>
            <Route path="" element={<ShoeList shoes={shoes} getShoes={getShoes} />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
